FROM node:18-alpine

WORKDIR /usr/src/biep
COPY . .

RUN apk update
RUN apk add --no-cache \
 python3 \
 g++ \
 build-base \
 cairo-dev \
 jpeg-dev \
 pango-dev \
 musl-dev \
 giflib-dev \
 pixman-dev \
 pangomm-dev \
 libjpeg-turbo-dev \
 freetype-dev \
 msttcorefonts-installer \
 fontconfig && \
 update-ms-fonts && \
 fc-cache -f

RUN npm install
RUN npm run compile

CMD [ "npm", "start" ]