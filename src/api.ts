import { Config } from "./config";
import { Entity } from "./entity";
import Misskey from "misskey-js";
import axios from "axios";
import FormData from "form-data";

export class API {
  private domain: string;
  private token: string;

  constructor(config: Config) {
    this.domain = config.domain;
    this.token = config.token;
  }

  public getBotAccount(): Promise<Misskey.entities.UserDetailed> {
    return new Promise((resolve, reject) => {
      axios.post(`https://${this.domain}/api/i`, { i: this.token }).then(res => resolve(res.data)).catch(err => reject(err));
    });
  }

  public getUsersById(ids: string[]): Promise<Misskey.entities.User[]> {
    return new Promise((resolve, reject) => {
      axios.post(`https://${this.domain}/api/users/show`, { 
        i: this.token, userIds: ids 
      }).then(res => resolve(res.data)).catch(err => reject(err));
    });
  }

  /*public getUserByNameAndHost(username: string, domain?: string, detail: boolean = false): Promise<Misskey.entities.User> {
    return new Promise((resolve, reject) => {
      //if (!domain) domain = this.domain;

      axios.post(`https://${this.domain}/api/users/search`, { query: username, limit: 1, detail }).then(users => {
        if (users.data.length) resolve(users.data[0]);

        axios.post(`https://${this.domain}/api/users/search-by-username-and-host`, { username, host: domain, limit: 1, detail }).then(users => {
          if (users.data.length) resolve(users.data[0]);
        
          resolve(null);
        }).catch(err => reject(err));
      }).catch(err => reject(err));
    });
  }*/

  public postNote(note: string, options?: Entity.PostNoteOptions): Promise<Misskey.entities.Note> {
    return new Promise((resolve, reject) => {
      options['i'] = this.token;
      options['text'] = note;

      axios.post(`https://${this.domain}/api/notes/create`, options)
        .then(res => resolve(res.data)).catch(err => reject(err));
    });
  }

  public uploadFile(buffer: Buffer, options: Entity.UploadFileOptions): Promise<Misskey.entities.DriveFile> {
    return new Promise((resolve, reject) => {
      const form = new FormData();
      form.append('i', this.token);
      form.append('file', buffer, options.name);
      form.append('name', options.name);
      if (options.folderId) form.append('folderId', options.folderId);
      if (options.comment) form.append('comment', options.comment);
      if (options.isSensitive) form.append('isSensitive', options.isSensitive);
      if (options.force) form.append('force', options.force);

      axios.post(`https://${this.domain}/api/drive/files/create`, form, {
        headers: { 'Content-Type': 'multipart/form-data' }
      }).then(res => resolve(res.data)).catch(err => reject(err));
    });
  }
}