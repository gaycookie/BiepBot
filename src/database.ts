import sqlite3 from "sqlite3";
import sqlite, { open } from "sqlite";

export interface CommandStat {
  note_id: string;
  author_id: string;
  command_name: string;
  timestamp: number;
}

export class Database {
  private db: sqlite.Database;

  constructor(path?: string) {
    open({
      filename: path || ":memory:",
      driver: sqlite3.Database
    }).then(db => {
      this.db = db;
    }).catch(console.error);
  }

  get connection(): sqlite.Database {
    return this.db;
  }

  public initialize(): Promise<sqlite.ISqlite.RunResult<sqlite3.Statement>> {
    return new Promise((resolve, reject) => {
      this.db.run("CREATE TABLE IF NOT EXISTS commands (note_id TEXT PRIMARY KEY, author_id TEXT, command_name TEXT, timestamp INTEGER);").then(value => resolve(value)).catch(err => reject(err));
    });
  }

  public addCommand(noteId: string, authorId: string, commandName: string): Promise<sqlite.ISqlite.RunResult<sqlite3.Statement>> {
    return new Promise((resolve, reject) => {
      const timestamp = Math.floor(Date.now() / 1000);
      this.db.run("INSERT INTO commands (note_id, author_id, command_name, timestamp) VALUES (?, ?, ?, ?);", [noteId, authorId, commandName, timestamp]).then(value => resolve(value)).catch(err => reject(err));
    });
  }

  public getCommands(): Promise<CommandStat[]> {
    return new Promise((resolve, reject) => {
      this.db.all("SELECT * FROM commands;", (err, res) => {
        if (err) reject(err);
        resolve(res);
      });
    })
  }
}