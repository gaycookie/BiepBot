import { Client } from ".";
import Misskey from "misskey-js";

export namespace Entity {
  export interface CommandFile {
    name: string;
    visibility: string;
    method: (client: Client, command: Command) => any;
  }

  export interface ModuleFile {
    name: string;
    method: (client: Client) => any;
  }

  export interface ICommand {
    author: Misskey.entities.User;
    note: Misskey.entities.Note;
    command_name: string;
    formatted_message: string;
    arguments: string[];
    mentions: Misskey.entities.User[];
  }

  export class Command {
    private client: Client;
    readonly author: Misskey.entities.User;
    readonly note: Misskey.entities.Note;
    readonly command_name: string;
    readonly formatted_message: string;
    readonly arguments: string[];
    readonly mentions: Misskey.entities.User[];

    constructor(client: Client, data: ICommand) { 
      this.client = client;
      Object.assign(this, data);
    }

    public reply(message: string, options: PostNoteOptions): Promise<Misskey.entities.Note> {
      return new Promise((resolve, reject) => {
        const formatMentions = [ `@${this.author.username}${this.author.host ? '@' + this.author.host : ''}` ];
        this.mentions.forEach(mention => formatMentions.push(`@${mention.username}${mention.host ? '@' + mention.host : ''}`));
        
        options.replyId = this.note.id; // Reply to the note that this command is in by default
        this.client.api.postNote(`${formatMentions.join(' ')} ${message}`, options).then(res => resolve(res)).catch(err => reject(err));
      });
    }
  }

  export interface UploadFileOptions {
    name: string;
    comment?: string;
    folderId?: string;
    isSensitive?: boolean;
    force?: boolean;
  }

  export interface PostNoteOptions {
    visibility?: "public" | "home" | "followers" | "specified";
    visibleUserIds?: string[];
    cw?: string;
    localOnly?: boolean;
    noExtractMentions?: boolean;
    noExtractHashtags?: boolean;
    noExtractEmojis?: boolean;
    fileIds?: string[];
    replyId?: string;
    channelId?: string;
    poll?: any; // TODO: Add poll type
  }
}