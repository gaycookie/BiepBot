import dotenv from "dotenv";
import config from "./config.json"

export class Config {
  /**
   * The domain the bot is member of.
   * @type {string}
   */
  readonly domain: string;
  
  /**
   * The token to connect to the instance API.
   * @type {string}
   */
  readonly token: string;

  /**
   * The username of the bot.
   * @type {string}
   */
  readonly username: string;

  /**
   * The prefix used to identify commands.
   * @type {string}
   */
  readonly prefix: string;

  /**
   * The cron expression used to run the daily stats module.
   * @type {string}
   */
  readonly cron: string;

  constructor() {
    dotenv.config();

    /**
     * Set the config values from env variables or the config file.
     * If env variables are not set, use the config file.
     * If the config file is not set, use the default values if available.
     */
    this.domain = process.env.DOMAIN || config.domain;
    this.token = process.env.TOKEN || config.token;
    this.username = process.env.USERNAME || config.username;
    this.prefix = process.env.PREFIX || config.prefix || "!";
    this.cron = process.env.CRON || config.cron || "0 0 * * *";
  }

  public isSet(value): boolean {
    return value !== undefined && value !== null && value !== "";
  }
}