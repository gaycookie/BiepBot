import { Entity } from "../entity";
import axios from "axios";
import filetype from "file-type";

export default {
  name: 'cat',
  visibility: 'public',
  method: (client, command) => {
    axios.get('https://cataas.com/cat?json=true').then(request => {
      axios.get(`https://cataas.com` + request.data.url, { responseType: 'arraybuffer' }).then(async image => {
        const fileBuffer = Buffer.from(image.data);
        const fileType = await filetype.fromBuffer(fileBuffer);
        const fileName = `${Date.now()}_${command.author.username}.${fileType.ext}`;

        client.api.uploadFile(fileBuffer, { name: fileName }).then(file => {
          command.reply('here is the random cat image you requested! :cool_felix:', {
            fileIds: [ file.id ],
          }).catch(console.error);
        }).catch(console.error);
      }).catch(console.error);
    }).catch(console.error);
  }
} as Entity.CommandFile;