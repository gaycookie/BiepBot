import { Entity } from "../entity";

const answers = [
  "It is certain", "It is decidedly so", "Without a doubt", "Yes, definitely", "You may rely on it",
  "As I see it, yes", "Most likely", "Outlook good", "Yes", "Signs point to yes", "Reply has been noted",
  "Ask again later", "Better not tell you now", "Cannot predict now", "Concentrate and ask again",
  "Don't count on it", "My reply is no", "My sources say no", "Outlook not so good", "Very doubtful"
];

export default {
  name: '8ball',
  visibility: 'public',
  method: (client, command) => {
    const answer = answers[Math.floor(Math.random() * answers.length)];
    command.reply(answer, {}).catch(console.error);
  }
} as Entity.CommandFile;