import { Entity } from "../entity";
import axios from "axios";
import filetype from "file-type";

export default {
  name: 'coffee',
  visibility: 'public',
  method: (client, command) => {
    axios.get('https://coffee.alexflipnote.dev/random.json').then(request => {
      axios.get(request.data.file, { responseType: 'arraybuffer' }).then(async image => {
        const fileBuffer = Buffer.from(image.data);
        const fileType = await filetype.fromBuffer(fileBuffer);
        const fileName = `${Date.now()}_${command.author.username}.${fileType.ext}`;

        client.api.uploadFile(fileBuffer, { name: fileName }).then(file => {
          command.reply('yummie yummie, coffee! :cool_felix:', {
            fileIds: [ file.id ],
          }).catch(console.error);
        }).catch(console.error);
      }).catch(console.error);
    }).catch(console.error);
  }
} as Entity.CommandFile;