import { Entity } from "../entity";
import axios from "axios";

export default {
  name: 'joke',
  visibility: 'public',
  method: (client, command) => {
    axios.get('https://v2.jokeapi.dev/joke/Any?blacklistFlags=nsfw,religious,political,racist,sexist,explicit&format=txt&type=single').then(request => {
      command.reply(`${request.data} 😂`, {}).catch(console.error);
    }).catch(console.error);
  }
} as Entity.CommandFile;