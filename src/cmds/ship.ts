import { Entity } from "../entity";
import { createCanvas, loadImage, registerFont } from "canvas";
import { createHash } from "crypto";
import path from "path";

export default {
  name: 'ship',
  visibility: 'public',
  method: async (client, command) => {
    if (!command.mentions.length) return;

    let user1 = command.author;
    let user2 = command.mentions[0];
    if (command.mentions.length > 1) {
      user1 = command.mentions[0];
      user2 = command.mentions[1];
    }

    const hashed_id1 = createHash('md5').update(user1.id).digest('hex');
    const hashed_id2 = createHash('md5').update(user2.id).digest('hex');
    const math = Math.abs(parseInt(hashed_id1, 16) - parseInt(hashed_id2, 16)) % 101;

    const canvas = createCanvas(960, 500);
    const context = canvas.getContext('2d');
    const background = await loadImage(path.join(__dirname, '..', '..', 'assets', 'ship', 'base.png'));
    const overlay = await loadImage(path.join(__dirname, '..', '..', 'assets', 'ship', 'overlay.png'));
    registerFont(path.join(__dirname, '..', '..', 'assets', 'fonts', 'TooFreakinCute.ttf'), { family: 'TooFreakinCute' });

    const avatar1 = await loadImage(user1.avatarUrl);
    const avatar2 = await loadImage(user2.avatarUrl);

    context.drawImage(background, 0, 0, canvas.width, canvas.height)
    context.drawImage(avatar1, 135, 56, 345, 330)
    context.drawImage(avatar2, 477.5, 56, 345, 330)
    context.drawImage(overlay, 0, 0, canvas.width, canvas.height)

    context.font = '48px TooFreakinCute'
    context.fillStyle = '#FFFFFF'
    context.fillText(`${math}%`, 620, 430)

    const buffer = canvas.toBuffer('image/png');
    const fileName = `${Date.now()}_${command.author.username}.png`;
    
    let message = ':heart_felix:';
    if (math >= 100) message = '#PerfectMatch #PerfectShip :heart_felix:';
    
    client.api.uploadFile(buffer, { name: fileName }).then(file => {
      command.reply(message, { 
        fileIds: [ file.id ] 
      }).catch(console.error);
    }).catch(console.error);
  }
} as Entity.CommandFile;