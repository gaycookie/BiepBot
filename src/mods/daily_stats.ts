import { Entity } from "../entity";
import cron from "node-cron";

export default {
  name: 'daily_stats',
  method: async (client) => {
    if (!client.config.cron || client.config.cron == '') {
      console.log('No cron set in config.json or env variables, skipping daily stats.');
      return;
    }

    if (!cron.validate(client.config.cron)) {
      console.log('Invalid cron set in config.json or env variables, skipping daily stats.');
      return;
    }

    cron.schedule(client.config.cron, async () => {
      const now = Math.floor(Date.now() / 1000)
      const day = now - (60 * 60 * 24);
  
      const oneDay = await client.database.connection.get(`SELECT COUNT(*) as count FROM commands WHERE timestamp > ${day}`);
      const oneDayUsers = await client.database.connection.get(`SELECT COUNT(DISTINCT author_id) as count FROM commands WHERE timestamp > ${day}`);
      const mostUsedToday = await client.database.connection.get(`SELECT COUNT(*) as count, command_name FROM commands WHERE timestamp > ${day} GROUP BY command_name ORDER BY count DESC LIMIT 1`); 
      const oneDayUser = await client.database.connection.get(`SELECT COUNT(*) AS count, author_id FROM commands WHERE timestamp > ${day} GROUP BY author_id ORDER BY count DESC LIMIT 1`);
      const [oneDayUserAcc] = await client.api.getUsersById([oneDayUser['author_id']]);
      const mention = `@${oneDayUserAcc.username}${oneDayUserAcc.host ? '@' + oneDayUserAcc.host : ''}`;
  
      let message = "\n";
      message += `Today **${oneDay['count']}** commands were used by **${oneDayUsers['count']}** users.\n`;
      message += `Todays most used command was **${mostUsedToday['command_name']}** with **${mostUsedToday['count']}** uses.\n`;
      message += `Todays user with the most commands used is **${mention}** with **${oneDayUser['count']}** commands.\n`;
      message += `This message was automatically generated.`;
  
      client.api.postNote(message, { visibility: 'public' });
    }, { scheduled: true });
  }
} as Entity.ModuleFile;