import { API } from "./api";
import { Config } from "./config";
import { Entity } from "./entity";
import { Database } from "./database";
import { Stream, ChannelConnection, entities } from "misskey-js";
import ws from "ws";
import fs from "fs";
import path from "path";


export class Client {
  public api: API;
  public socket: Stream;
  public self: entities.UserDetailed;
  public channel: ChannelConnection;
  public config: Config = new Config();
  public commands: Map<string, Entity.CommandFile> = new Map();
  public database: Database = new Database('database.sqlite');

  constructor() {
    this.checkConfigEntries();
    this.api = new API(this.config);

    this.api.getBotAccount().then(bot => {
      this.self = bot;
      console.log(`Logged in as ${bot.name}!`);

      this.database.initialize().then(() => {
        this.socket = new Stream(`https://${this.config.domain}`, { token: this.config.token }, { WebSocket: ws });
        this.channel = this.socket.useChannel("main");
  
        this.initCommands();
        this.initModules();
        this.initEvents();
      }).catch(err => { console.error(err); process.exit(1) });
    }).catch(err => { console.error(err); process.exit(1) });
  }

  private checkConfigEntries(): void {
    if (!this.config.isSet(this.config.domain)) {
      console.error('No domain set in config.json or env variables.');
      process.exit(1);
    }

    if (!this.config.isSet(this.config.token)) {
      console.error('No token set in config.json or env variables.');
      process.exit(1);
    }

    if (!this.config.isSet(this.config.username)) {
      console.error('No bot username set in config.json or env variables.');
      process.exit(1);
    }
  }

  private initModules(): void {
    fs.readdirSync(path.join(__dirname, 'mods')).forEach(file => {
      import(`./mods/${file}`).then(mod => mod.default.method(this));
    });
  }

  private initCommands(): void {
    fs.readdirSync(path.join(__dirname, 'cmds')).forEach(file => {
      import(`./cmds/${file}`).then(cmd => this.commands.set(cmd.default.name.toLocaleLowerCase(), cmd.default));
    });
  }

  private initEvents(): void {
    this.socket.on('_connected_', () => console.log('Connected to the Fediverse! 🦄'));
    this.socket.on('_disconnected_', () => console.log('Disconnected from the Fediverse! 😭'));

    this.channel.on('notification', async (notification: entities.Notification) => {
      if (notification.type !== 'mention') return;
    
      const message = notification.note.text;
      const splittedMessage = message.split(' ');

      let mentions = await this.api.getUsersById(notification.note['mentions']);
      if (mentions.length) mentions = mentions.filter(user => user.id !== this.self.id);
      const messageWithoutMentions = splittedMessage.filter(arg => !arg.match(/@\S+/g));
      const args = messageWithoutMentions.slice(1);
      
      const messageStartsWithPrefix = messageWithoutMentions.length ? messageWithoutMentions[0].startsWith(this.config.prefix) : null;
      if (!messageStartsWithPrefix) return;

      const commandFile = this.commands.get(messageWithoutMentions[0].slice(1).toLocaleLowerCase());
      if (!commandFile) return;      

      const command = new Entity.Command(this, {
        author: notification.user,
        note: notification.note,
        command_name: commandFile.name,
        formatted_message: args.join(' '),
        arguments: args,
        mentions: mentions
      });

      this.database.addCommand(command.note.id, command.author.id, command.command_name).then(() => {
        commandFile.method(this, command);
      }).catch(console.error);
      
      return;
    });
  }
}

new Client();