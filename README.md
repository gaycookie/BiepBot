# BiepBot
[![License](https://img.shields.io/badge/license-WTFPL-brightgreen?style=for-the-badge)](LICENSE) 
‎‎ [![Docker Pulls](https://img.shields.io/docker/pulls/gaycookie/biepbot?style=for-the-badge)](https://hub.docker.com/r/gaycookie/biepbot)
‎‎ [![Fediverse Followers](https://img.shields.io/endpoint?url=https://gaycookie.dev/misskey.php?userId=8zpkico32u&style=for-the-badge)](https://mk.gaycookie.dev/@cookie)

## What is BiepBot? 
BiepBot is a bot that is developed for the [Fediverse] and uses [Misskey].  
And is written in Javascript (Node.js) and uses the [Misskey.js] library for the types and sockets.

## Using BiepBot
BiepBot is available by cloning the repository and running the bot locally.  
You can also find a Docker image for BiepBot [here](https://hub.docker.com/r/gaycookie/biepbot)

## Build from source
You can build BiepBot from source by running the following commands:
* `git clone https://git.gaycookie.dev/GayCookie/BiepBot.git`
* `cd BiepBot`
* `npm install`
* `npm run compile`
* `npm run start`

To build the bot for Docker, run the following commands:
* `git clone https://git.gaycookie.dev/GayCookie/BiepBot.git`
* `cd BiepBot`
* `docker build -t biepbot .`
* `docker run -e DOMAIN="<domain>" -e TOKEN="<token>" -e USERNAME="<username>" -d biepbot`

## Configuration
BiepBot uses two configuration systems:
* .env / env variables - Environment variables file
* [config.json](src/config.json) - Configuration file

Both can be used and even at the same time but the .env file will be used first.  
So if you want to use the [config.json](src/config.json) file, you'll need to remove the .env file / env variables.  

If you decide to use the .env file or variables, you use the following variables:
```
DOMAIN="<domain>"
TOKEN="<token>"
USERNAME="<username>"
PREFIX="<prefix>"
CRON="<cronjob expression>"
```

## Commands
| Command   | Arguments              | Description |
|:----------|------------------------|-------------|
| `!8ball`  | `<question>`           | Ask the magic 8 ball a question. |
| `!cat`    |                        | Get a random cat picture. |
| `!coffee` |                        | Get a random coffee picture. |
| `!dog`    |                        | Get a random dog picture. |
| `!joke`   |                        | Get a random joke. |
| `!ship`   | `<mention> <mention>?` | Ship yourself with someone or ship two people together. |

[Misskey]: https://misskey-hub.net/en
[Fediverse]: https://fediverse.party
[Misskey.js]: https://github.com/misskey-dev/misskey.js